<?php

/**
 * @file
 * Contains offline_app.module.
 */

use Drupal\Core\Extension\Extension;
use Drupal\node\NodeInterface;
use Drupal\offline_app\OfflineApp;

/**
 * Implements hook_theme().
 */
function offline_app_theme() {
  return [
    'offline_app_html' => [
      'render element' => 'html',
    ],
    'offline_app_page' => [
      'render element' => 'page',
    ],
    'offline_app_system_branding_block' => [
      'render element' => 'elements',
      'base hook' => 'block',
    ],
  ];
}

/**
 * Implements hook_page_attachments.
 */
function offline_app_page_attachments(&$attachments) {
  $config = \Drupal::config('offline_app.homescreen');

  if (!$config->get('online_pages')) {
    return;
  }

  $chrome = $config->get('chrome');
  $safari = $config->get('safari');
  $icon = $config->get('icon_192');
  $icon_type = $config->get('icon_192_type');

  // Add Chrome Homescreen.
  if ($chrome) {
    $attachments['#attached']['html_head'][] = [
      [
        '#tag' => 'meta',
        '#attributes' => [
          'name' => 'mobile-web-app-capable',
          'content' => 'yes'
        ],
      ],
      'homescreen_chrome',
    ];

    if ($icon && $icon_type) {
      $attachments['#attached']['html_head'][] = [
        [
          '#tag' => 'link',
          '#attributes' => [
            'rel' => 'manifest',
            'href' => '/manifest.json',
          ],
        ],
        'homescreen_manifest'
      ];
    }
  }

  // Add Safari Homescreen.
  if ($safari) {
    $attachments['#attached']['html_head'][] = [
      [
        '#tag' => 'meta',
        '#attributes' => [
          'name' => 'apple-mobile-web-app-capable',
          'content' => 'yes'
        ],
      ],
      'homescreen_safari',
    ];

    // Add 192x192 launcher and startup icon.
    if ($icon && $icon_type) {

      $attachments['#attached']['html_head'][] = [
        [
          '#tag' => 'link',
          '#attributes' => [
            'rel' => 'apple-touch-icon',
            'href' => $icon,
          ],
        ],
        'homescreen_safari_192_touch_icon',
      ];

      $attachments['#attached']['html_head'][] = [
        [
          '#tag' => 'link',
          '#attributes' => [
            'rel' => 'apple-touch-startup-image',
            'href' => $icon,
          ],
        ],
        'homescreen_safari_192_icon',
      ];
    }

  }

}

/**
 * Implements hook_system_info_alter().
 */
function offline_app_system_info_alter(array &$info, Extension $file, $type) {
  // Add offline_header and offline_footer regions.
  if ($type == 'theme') {
    $info['regions']['offline_header'] = 'Offline header';
    $info['regions']['offline_content'] = 'Offline content';
    $info['regions']['offline_footer'] = 'Offline footer';
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function offline_app_theme_suggestions_html_alter(array &$suggestions, array $variables) {
  if (OfflineApp::isOfflineRequest()) {
    $suggestions = ['offline_app_html'];
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function offline_app_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  if (OfflineApp::isOfflineRequest()) {
    $suggestions = ['offline_app_page'];
  }
}

function offline_app_theme_suggestions_block_alter(array &$suggestions, array &$variables) {
  if (OfflineApp::isOfflineRequest() && $variables['elements']['#base_plugin_id'] == 'system_branding_block') {
    $suggestions = ['offline_app_system_branding_block'];
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function offline_app_preprocess_node(&$variables) {
  if (OfflineApp::isOfflineRequest()) {
    if ($variables['view_mode'] == 'offline_full') {
      $variables['page'] = TRUE;
    }

    // Rewrite 'url'.
    if (in_array($variables['view_mode'], ['offline_teaser', 'teaser'])) {
      /** @var \Drupal\node\NodeInterface $node */
      $node = $variables['node'];
      if ($offline_alias = OfflineApp::getOfflineNodeAlias($node->id())) {
        $variables['url'] = $offline_alias;
      }
    }
  }
}

/**
 * Implements hook_node_links_alter().
 */
function offline_app_node_links_alter(array &$links, NodeInterface $entity, array &$context) {

  // Change read more url.
  if ($context['view_mode'] == 'teaser' && isset($links['node']['#links']['node-readmore'])) {
    $links['node']['#links']['node-readmore']['url'] = OfflineApp::getOfflineNodeAlias($entity->id(), FALSE);
  }

  // Add read more link for offline_teaser.
  if (\Drupal::config('offline_app.appcache')->get('expose_read_more') && $context['view_mode'] == 'offline_teaser') {
    $node_title_stripped = strip_tags($entity->label());
    $links['node']['#links']['node-readmore'] = [
      'title' => t('Read more<span class="visually-hidden"> about @title</span>', [
        '@title' => $node_title_stripped,
      ]),
      'url' => OfflineApp::getOfflineNodeAlias($entity->id(), FALSE),
      'language' => $entity->language(),
      'attributes' => [
        'rel' => 'tag',
        'title' => $node_title_stripped,
      ],
    ];
  }
}

/**
 * Preprocess function for offline-app-system-branding-block.
 */
function template_preprocess_offline_app_system_branding_block(&$variables) {
  $content = $variables['elements']['content'];
  $variables['site_logo'] = '';
  if ($content['site_logo']['#access'] && $content['site_logo']['#uri']) {
    $variables['site_logo'] = $content['site_logo']['#uri'];
  }
  $variables['site_name'] = '';
  if ($content['site_name']['#access'] && $content['site_name']['#markup']) {
    $variables['site_name'] = $content['site_name']['#markup'];
  }
  $variables['site_slogan'] = '';
  if ($content['site_slogan']['#access'] && $content['site_slogan']['#markup']) {
    $variables['site_slogan']['#markup'] = $content['site_slogan']['#markup'];
  }
}

/**
 * Preprocess function for offline-app-html.
 */
function template_preprocess_offline_app_html(&$variables) {
  template_preprocess_html($variables);

  // Add offline-frontpage class and optionally the manifest attribute.
  if (OfflineApp::isOfflineHomepage()) {
    $variables['attributes']['class'][] = 'offline-frontpage';
  }

  // Add manifest attribute on offline pages.
  if (OfflineApp::isOfflineRequest() && \Drupal::config('offline_app.appcache')->get('manifest.tag_on_offline')) {
    $variables['html_attributes']['manifest'] = '/manifest.appcache';
  }

  // Add the javascript.
  $javascript = explode("\n", trim(\Drupal::config('offline_app.appcache')->get('javascript')));
  if (!empty($javascript)) {
    $javascript_markup = '';
    foreach ($javascript as $js) {
      $js = trim($js);
      if (!empty($js)) {
        $javascript_markup .= '<script type="text/javascript" src="' . $js . '"></script>' . "\n";
      }
    }

    if (!empty($javascript_markup)) {
      $javascript_build = [
        '#markup' => $javascript_markup,
        '#allowed_tags' => ['script'],
      ];
      $variables['offline_javascript'] = \Drupal::service('renderer')->render($javascript_build);
    }
  }

  // Add the stylesheets.
  $stylesheets = explode("\n", trim(\Drupal::config('offline_app.appcache')->get('stylesheets')));
  if (!empty($stylesheets)) {
    $stylesheet_markup = '';
    foreach ($stylesheets as $stylesheet) {
      $stylesheet = trim($stylesheet);
      if (!empty($stylesheet)) {
        $stylesheet_markup .= '<link rel="stylesheet" type="text/css" href="' . $stylesheet . '" />' . "\n";
      }
    }

    if (!empty($stylesheet_markup)) {
      $stylesheet_build = [
        '#markup' => $stylesheet_markup,
        '#allowed_tags' => ['link'],
      ];
      $variables['offline_stylesheet'] = \Drupal::service('renderer')->render($stylesheet_build);
    }
  }

  // Offline meta on offline pages.
  if (OfflineApp::isOfflineRequest() && \Drupal::config('offline_app.homescreen')->get('offline_pages')) {
    $meta_build = [];

    $config = \Drupal::config('offline_app.homescreen');
    $chrome = $config->get('chrome');
    $safari = $config->get('safari');
    $icon = $config->get('icon_192');
    $icon_type = $config->get('icon_192_type');

    if ($chrome) {
      $meta_build['chrome'] = [
        '#markup' => '<meta name="mobile-web-app-capable" content="yes" />',
        '#allowed_tags' => ['meta'],
      ];

      if ($icon && $icon_type) {
        $meta_build['chrome_manifest'] = [
          '#markup' => '<link rel="manifest" href="/manifest.json" />',
          '#allowed_tags' => ['link'],
        ];
      }
    }

    if ($safari) {
      $meta_build['safari'] = [
        '#markup' => '<meta name="apple-mobile-web-app-capable" content="yes" />',
        '#allowed_tags' => ['meta'],
      ];

      if  ($icon) {
        $meta_build['safari_touch'] = [
          '#markup' => '<link rel="apple-touch-icon" href="' . $icon . '" />',
          '#allowed_tags' => ['link'],
        ];

        $meta_build['safari_touch_startup_image'] = [
          '#markup' => '<link rel="apple-touch-startup-image" href="' . $icon . '" />',
          '#allowed_tags' => ['link'],
        ];
      }

    }

    $variables['offline_meta'] = \Drupal::service('renderer')
      ->render($meta_build);
  }

}

/**
 * Preprocess function for offline-app-page.
 */
function template_preprocess_offline_app_page(&$variables) {
  $config = \Drupal::config('offline_app.appcache');
  $markup = '<div id="offline-messages-first-time-text" content="' . $config->get('first_time_text') . '"></div>';
  $markup .= '<div id="offline-messages-update-ready-text" content="' . $config->get('update_ready_text') . '"></div>';

  $offline_messages_build = [
    '#markup' => $markup,
    '#allowed_tags' => ['div'],
  ];

  $variables['offline_messages_data'] = $offline_messages_build;
}
